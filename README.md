# README #

Completed the following tasks:

1. Define a database table in a custom module (completed!)
    - after 'plusone' module is enabled 'plusone_votes' table is created

2. Write a Drupal JS behavior that shows an alert box whenever a submit button is clicked (completed!)
    - after 'plusone' module is enabled, proceed to the page '/plusone/submit-behavior' to see the result

3. Create an Ajax enabled custom form using only Drupal's API (completed!)
    - after 'plusone' module is enabled, proceed to the module settings page 'admin/config/development/plusone' to see the form with the Drupal ajax functionality (using only Drupal's API) that stores settings to the 'variable' table

OPTIONAL:
I also added some optional functionality within this module, just to link three tasks into one context:
  - I added custom voting to the 'full view' node.
  - Choose the node types where you want voting functionality to be available at 'admin/config/development/plusone'