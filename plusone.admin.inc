<?php
/**
 * @file
 * Settings for module plusone
 *
 * List all enabled content types
 * and allow administrator to choose what content types
 * he wants plusone voting Button to appear
 */

/**
 * Plusone admin settings form.
 */
function plusone_admin_settings() {
  $form['plusone_alert'] = array(
    '#type' => 'item',
    '#title' => t('The list of all available node types:'),
    '#markup' => '<div>Choose the node types, you want voting button to appear.</div>',
    '#prefix' => '<div id="plusone-submit-alert">',
    '#suffix' => '</div>',
  );

  $node_types = node_type_get_types();
  foreach (element_children($node_types) as $type) {
    if(!$node_types[$type]->disabled) {
      $form['plusone_nodetype_' . $type] = array(
        '#type' => 'checkbox',
        '#title' => t('Node type: @nodetype', array('@nodetype' => $node_types[$type]->name)),
        '#default_value' => variable_get('plusone_nodetype_' . $type, FALSE),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'plusone_admin_settings_callback',
      'wrapper' => 'plusone-submit-alert',
    ),
    '#value' => t('Save settings'),
  );
  return $form;
}


/**
 * Callback for plusone_admin_settings.
 */
function plusone_admin_settings_callback($form, &$form_state) {
  $element = $form['plusone_alert'];
  $element['#markup'] = "Thank you! Settings saved successfully!";
  // process the form
  plusone_admin_settings_submit($form, $form_state);
  // return the confirmation message
  return $element;
}


/**
 * Submit handler for plusone_admin_settings
 * we call this function from plusone_admin_settings_callback
 */
function plusone_admin_settings_submit($form, &$form_state) {
  foreach (element_children($form_state['values']) as $keys) {
    if(drupal_substr($keys,0,7) == 'plusone') {
      variable_set($keys, $form_state['values'][$keys]);
    }
  }
}