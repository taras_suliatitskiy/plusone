<?php
/**
 * @file
 * Template for displaying the voting
 */

// Add some javascript and css
drupal_add_js(drupal_get_path('module', 'plusone') . '/js/plusone.js');
drupal_add_css(drupal_get_path('module', 'plusone') . '/css/plusone.css');

// prepare the output
$output = '<div class="plusone-widget">';
$output .= '<div class="score">'. $total .'</div>';

$output .= '<div class="vote">';
// Based on the arguments, passed from the theme_plusone_widget() – display the appropriate result
// below the vote count.
if ($is_author || !user_access('rate content')) {
  // User is author of content; that's why he's not allowed to vote.
  $output .= t('Votes');
} elseif ($voted > 0) {
  // Not allowed to vote again, since user already voted for this node
  $output .= t('You Voted');
} else {
  // Go ahead and vote.
  $output .= l(t('Vote'), "plusone/vote/$nid", array(
    'attributes' => array('class' => 'plusone-link')
  ));
}
$output .= '</div>';
// Closing div with class 'vote'
$output .= '</div>';
// Closing div with class 'plusone-widget'
print $output;