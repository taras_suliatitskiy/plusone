(function($) {
   Drupal.behaviors.plusone_button_alert = {
     attach: function (context, settings) {
       $('.apply-behaviors', context).click(function (event) {
         event.preventDefault();  // Just in case:)
         alert(Drupal.t('You did it!'));
       });
     }
   };
})(jQuery);