(function($) {
   Drupal.behaviors.plusone = {
     attach: function (context, settings) {
       $('a.plusone-link:not(.plusone-processed)', context).click(function (event) {
         event.preventDefault();
         var voteSaved = function (data) {
            // Update the number of votes.
            jQuery('div.score').html(data.total_votes);
            // Update the "Vote" string to "You voted".
            jQuery('div.vote').html(data.voted);
         }
         jQuery.ajax({
            type: 'POST', // Use the POST method.
            url: this.href,
            dataType: 'json',
            success: voteSaved,
            data: 'js=1'  // Pass a key/value pair.
         });
       }).addClass('plusone-processed');
     }
   };
})(jQuery);

